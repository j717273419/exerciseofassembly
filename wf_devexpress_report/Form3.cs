﻿using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace wf_devexpress_report
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
            
            //report.Dispose();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            //获取datatable数据
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("student");
            ds.Tables.Add(dt);
            DataColumn dc1 = new DataColumn("username", typeof(string));
            DataColumn dc2 = new DataColumn("age", typeof(Int32));
            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);
            DataRow dr1 = dt.NewRow();

            dr1["username"] = "daohe";
            dr1["age"] = 22;
            dt.Rows.Add(dr1);

            //加载数据，用户自定义报表格式
            DevExpress.XtraReports.UI.XtraReport report = new DevExpress.XtraReports.UI.XtraReport();
            report.LoadLayout(Application.StartupPath + @"\ReportFiles\XtraReportDemo.repx");
            report.DataSource = ds;
            report.DataMember = "student";
            report.Print();
            
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }
    }
}
