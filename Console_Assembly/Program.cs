﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Console_Assembly
{
    class Program
    {
        static void Main(string[] args)
        {
            bool result = false;
            if (File.Exists(@"..\..\..\ClassLibrary_Assembly\bin\Debug\ClassLibrary_Assembly.dll"))
            {
                result = true;
            }
            Console.WriteLine(result);
            //Assembly assembly = Assembly.Load("ClassLibrary_Assembly");
            Assembly assembly = Assembly.LoadFrom("ClassLibrary_Assembly.DLL");
            Type type = assembly.GetType("ClassLibrary_Assembly.TestClass");
            object instance = assembly.CreateInstance("ClassLibrary_Assembly.TestClass");
            Type[] params_type = new Type[1];
            params_type[0] = Type.GetType("System.String");
            Object[] params_obj = new Object[1];
            params_obj[0] = "jjjaa";
            object value = type.GetMethod("Show_UserName", params_type).Invoke(instance, params_obj);





            //Console_AssemblyExe
            Console.WriteLine("=========== Console_AssemblyExe =============");
            Assembly assemblyExe = Assembly.LoadFrom("Console_AssemblyExe.exe");
            Type typeProgram = assemblyExe.GetType("Console_AssemblyExe.Program");
            object instanceExe = assemblyExe.CreateInstance("Console_AssemblyExe.Program");

            object valueExe = typeProgram.GetMethod("test").Invoke(instanceExe, null);
            //通过Reflection获取EXE文件中对象的字段和字段值
            //FieldInfo
            foreach (var item in instanceExe.GetType().GetFields())
            {
                //获取字段的Attribute
                DescriptionAttribute descriptionAttribute = Attribute.GetCustomAttribute(item, typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (descriptionAttribute != null)
                {
                    Console.Write("Attribute:" + descriptionAttribute.Description + ",");
                }
                //获取字段和字段的值
                Console.WriteLine(item.Name + ":" + item.GetValue(instanceExe));

            }
            Console.ReadLine();
        }
    }
}
