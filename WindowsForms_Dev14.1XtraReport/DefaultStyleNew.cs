﻿using System.Drawing.Printing;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.UI;
using KingT.PEIS.Model.DataSet;

namespace KingT.PEIS.Report
{
    public partial class DefaultStyleNew : XtraReport
    {
        public DefaultStyleNew()
        {
            InitializeComponent();
        }

        private void Detail_BeforePrint(object sender, PrintEventArgs e)
        {
            if (DesignMode)
            {
                DataSource = PersonalReportData.CreatePreviewData();
            }
            DefaultGeneralProjectNew generalProject = subRepGeneral.ReportSource as DefaultGeneralProjectNew;
            if (generalProject != null)
            {
                generalProject.DataSource = DataSource;
                generalProject.FillDataSource();
            }

            DefaultPacsProjectNew pacsProject = subRepPacs.ReportSource as DefaultPacsProjectNew;
            if (pacsProject != null)
            {
                pacsProject.DataSource = DataSource;
                pacsProject.FillDataSource();
            }
            DefaultLisProjectNew lisProject = subRepLis.ReportSource as DefaultLisProjectNew;
            if (lisProject != null)
            {
                lisProject.DataSource = DataSource;
                lisProject.FillDataSource();
            }
        }

        /// <summary>
        /// 给报表格式赋值
        /// </summary>
        /// <param name="general">普通检查项目格式</param>
        /// <param name="lis">化验检查项目格式</param>
        /// <param name="pacs">功能检查项目格式</param>
        public void SetLayout(Stream general, Stream lis, Stream pacs)
        {
            subRepGeneral.ReportSource.LoadLayout(general);
            subRepLis.ReportSource.LoadLayout(lis);
            subRepPacs.ReportSource.LoadLayout(pacs);
        }

        /// <summary>
        /// 普通项目报表数据源
        /// </summary>
        public DefaultGeneralProjectNew GeneralReportSource
        {
            get
            {
                return subRepGeneral.ReportSource as DefaultGeneralProjectNew;
            }

            set
            {
                subRepGeneral.ReportSource = value;
            }
        }

        /// <summary>
        /// 功能项目报表数据源
        /// </summary>
        public DefaultPacsProjectNew PacsReportSource
        {
            get
            {
                return subRepPacs.ReportSource as DefaultPacsProjectNew;
            }

            set
            {
                subRepPacs.ReportSource = value;
            }
        }

        /// <summary>
        /// 检验项目报表数据源
        /// </summary>
        public DefaultLisProjectNew LisReportSource
        {
            get
            {
                return subRepLis.ReportSource as DefaultLisProjectNew;
            }

            set
            {
                subRepLis.ReportSource = value;
            }
        }

        /// <summary>
        /// 设置报表显示名称
        /// </summary>
        /// <param name="name">主报表名称</param>
        /// <param name="sGeneralName">普通项目报表名称</param>
        /// <param name="sPacsName">功能项目报表名称</param>
        /// <param name="sLisName">检验项目报表名称</param>
        public void SetName(string name, string sGeneralName, string sPacsName, string sLisName)
        {
            DisplayName = name;
            subRepGeneral.ReportSource.DisplayName = sGeneralName;
            subRepLis.ReportSource.DisplayName = sLisName;
            subRepPacs.ReportSource.DisplayName = sPacsName;
        }
        /// <summary>
        /// 检查数据，主要是有没有HospitalName和有没有3个子报表
        /// </summary>
        /// <returns></returns>
        public bool CheckData()
        {
            if (Parameters.GetByName("HospitalName") == null)
            {
                XtraMessageBox.Show("医院名称参数不能删除！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (FindControl("subRepGeneral", true) == null)
            {
                XtraMessageBox.Show("子报表subRepGeneral不能删除！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            if (FindControl("subRepLis", true) == null)
            {
                XtraMessageBox.Show("子报表subRepLis不能删除！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            if (FindControl("subRepPacs", true) == null)
            {
                XtraMessageBox.Show("子报表subRepPacs不能删除！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

    }
}
