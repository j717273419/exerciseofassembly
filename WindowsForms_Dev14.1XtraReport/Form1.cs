﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsForms_Dev14._1XtraReport
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void simpleButton_OpenReport_Click(object sender, EventArgs e)
        {
            DefaultGeneralProjectNew report = new DefaultGeneralProjectNew();
            report.DataSource = PersonalReportDataSet.CreatePreviewData();
            report.ShowPreviewDialog();
        }

        public DataSet getData()
        {
            for (int i = 0; i < 100; i++)
            {
                DataSet1.Tables[0].Rows.Add("");
            }
            return DataSet1;
        }
    }
}
