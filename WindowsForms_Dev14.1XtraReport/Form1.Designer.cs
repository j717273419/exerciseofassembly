﻿namespace WindowsForms_Dev14._1XtraReport
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButton_OpenReport = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // simpleButton_OpenReport
            // 
            this.simpleButton_OpenReport.Location = new System.Drawing.Point(163, 133);
            this.simpleButton_OpenReport.Name = "simpleButton_OpenReport";
            this.simpleButton_OpenReport.Size = new System.Drawing.Size(245, 51);
            this.simpleButton_OpenReport.TabIndex = 0;
            this.simpleButton_OpenReport.Text = "打开报表";
            this.simpleButton_OpenReport.Click += new System.EventHandler(this.simpleButton_OpenReport_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(607, 342);
            this.Controls.Add(this.simpleButton_OpenReport);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton_OpenReport;
    }
}

