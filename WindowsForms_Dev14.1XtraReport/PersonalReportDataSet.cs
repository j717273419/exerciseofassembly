﻿using DevExpress.Data;
using System;

namespace WindowsForms_Dev14._1XtraReport
{


    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2240:ImplementISerializableCorrectly")]
    public partial class PersonalReportDataSet : IDisplayNameProvider
    {
        partial class ProjectInfoDataTable
        {
        }

        partial class DiagnosisInfoDataTable
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static PersonalReportDataSet CreatePreviewData()
        {
            PersonalReportDataSet result = new PersonalReportDataSet();

            result.PersonalInfo.AddPersonalInfoRow("72450", "200912290001", "何志华", "男", "35", "天然气公司", "常规体检", "职工体检套餐", @"1.腹：右侧[×]
2.心：左侧[×]
3.心律：在正常范围[×]
4.脾：光滑[×]
5.肺部：右肺罗音[×]
", "无", "系统管理", "2011/10/08", "严冷余;卢牡士;高任廷", "", "0574-12345678", "13912345678", "", "", Convert.ToDateTime("1980-03-01"), "2011/10/08", @"总建议");

            result.DeptInfo.AddDeptInfoRow(result.PersonalInfo[0], "体检眼科", "黎雪君", "2011/10/08", 10, "未见明显异常");
            //result.ProjectInfo.AddProjectInfoRow(result.DeptInfo[0], 0m, "眼科检查", "1", "", "", "", "", "", "0");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[0], 0m, "未检", "结膜", "72450", "", "", "", "");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[0], 1m, "未检", "辨色", "72450", "", "", "", "");

            result.DeptInfo.AddDeptInfoRow(result.PersonalInfo[0], "内外科", "何林法", "2011/10/08", 11, "未见明显异常");
           // result.ProjectInfo.AddProjectInfoRow(result.DeptInfo[1], 1m, "常规五项(女)", "1", "系统管理", "2012-07-31", "P:0", "无", "系统管理", "0");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[1], 2m, "90", "收缩压(mmHg)", "72450", "mmHg", "", "", "");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[1], 3m, "60", "舒张压(mmHg)", "72450", "mmHg", "", "", "");

            //result.ProjectInfo.AddProjectInfoRow(result.DeptInfo[1], 2m, "内科检查", "1", "2011/10/08", "2011/10/08", "P:0", "无", "何林法", "0");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[2], 4m, "未检", "脉搏", "72450", "次/分钟", "", "", "");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[2], 5m, "未检", "血压", "72450", "mmHg", "", "", "");

            result.DeptInfo.AddDeptInfoRow(result.PersonalInfo[0], "院办", "何林法", "2011/10/08", 12, "未见明显异常");
           // result.ProjectInfo.AddProjectInfoRow(result.DeptInfo[2], 3m, "常规五项(女)", "1", "系统管理", "2012-07-31", "P:0", "无", "系统管理", "0");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[3], 6m, "160", "身高(cm)", "72450", "cm", "", "", "");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[3], 7m, "50", "体重(kg)", "72450", "kg", "", "", "");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[3], 8m, "19", "体重指数（BMI）", "72450", "", "", "", "");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[3], 9m, "30", "脂肪率(女)(%)", "72450", "%", "", "", "");

            result.DeptInfo.AddDeptInfoRow(result.PersonalInfo[0], "体检妇科", "", "2011/10/08", 14, "");
           // result.ProjectInfo.AddProjectInfoRow(result.DeptInfo[3], 4m, "脑CT平扫", "2", "", "", "", "", "", "0");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[4], 10m, "未检", "头颅CT", "72450", "", "", "", "");

            result.DeptInfo.AddDeptInfoRow(result.PersonalInfo[0], "B超", "", "2011/10/08", 15, "");
            //result.ProjectInfo.AddProjectInfoRow(result.DeptInfo[4], 5m, "肝胆肾B超", "2", "", "", "", "", "", "0");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[5], 11m, "未检", "胆", "72450", "", "", "", "");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[5], 12m, "未检", "肾", "72450", "", "", "", "");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[5], 13m, "未检", "肝", "72450", "", "", "", "");

            result.DeptInfo.AddDeptInfoRow(result.PersonalInfo[0], "化验科", "", "2011/10/08", 16, "");
           // result.ProjectInfo.AddProjectInfoRow(result.DeptInfo[5], 6m, "甲状腺功能", "3", "", "", "", "", "", "0");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[6], 14m, "", "三碘甲腺原氨酸", "72450", "ng/ml", ".9-1.8", "", "");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[6], 15m, "", "甲状腺素", "72450", "ng/ml", "53-115", "", "");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[6], 16m, "", "游离T3", "72450", "pg/mL", "2.4-6.8", "", "");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[6], 17m, "", "游离T4", "72450", "pg/ml", "8.7-15.6", "", "");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[6], 18m, "", "促甲状腺素", "72450", "uIu/ml", ".34-5.6", "", "");

            //result.ProjectInfo.AddProjectInfoRow(result.DeptInfo[5], 7m, "肿瘤标志物四项[女]", "3", "", "", "", "", "", "0");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[7], 19m, "", "甲胎蛋白(AFP)", "72450", "ng/ml", "0-10", "", "");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[7], 20m, "", "卵巢癌抗原(CA-125)", "72450", "u/ml", "0-35", "", "");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[7], 21m, "", "癌胚抗原(CEA)", "72450", "ng/ml", "0-10", "", "");
            result.TargetInfo.AddTargetInfoRow(result.ProjectInfo[7], 22m, "", "胰腺癌异性抗原(CA199)", "72450", "", "0-37", "", "");

            result.DiagnosisInfo.AddDiagnosisInfoRow(result.PersonalInfo[0], "结膜翼状胬肉", "描述：轻度", "健康处方", "建议复查", "");
            return result;
        }


        /// <summary>
        /// <para>
        /// Returns the name displayed for a datasource in a Field List.
        /// </para>
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> value specifying the datasource's name.
        /// </returns>
        public string GetDataSourceDisplayName()
        {
            return "个人报表数据源";
        }


        /// <summary>
        /// <para>
        /// Returns the name of a table or column item displayed in a Field List.
        /// </para>
        /// </summary>
        /// <param name="fieldAccessors">An array of <see cref="T:System.String"/> values, corresponding to the original data member display names. 
        ///             </param>
        /// <returns>
        /// A <see cref="T:System.String"/> value, specifying the new data member name.
        /// </returns>
        public string GetFieldDisplayName(string[] fieldAccessors)
        {
            if (fieldAccessors == null)
            {
                throw new ArgumentNullException("fieldAccessors", "fieldAccessors为空");
            }
            string fieldName = fieldAccessors[fieldAccessors.Length - 1];
            if (fieldAccessors.Length > 2)
            {
                var rel = fieldAccessors[fieldAccessors.Length - 2];
                var tableName = rel.Substring(rel.IndexOf("_", StringComparison.Ordinal) + 1, rel.Length - rel.IndexOf("_", StringComparison.Ordinal) - 1);
                return GetCaption(fieldName, tableName);
            }
            return GetCaption(fieldName, fieldAccessors[0]);
        }

        /// <summary>
        /// 获取显示名称
        /// </summary>
        /// <param name="fieldName">字段名</param>
        /// <param name="tableName">表名</param>
        /// <returns>显示名称</returns>
        private string GetCaption(string fieldName, string tableName)
        {
            // 传入的是表名
            if (Tables.Contains(fieldName))
            {
                switch (fieldName)
                {
                    case "PersonalInfo":
                        return "个人信息";
                    case "DiagnosisInfo":
                        return "诊断信息";
                    case "DeptInfo":
                        return "科室信息";
                    case "ProjectInfo":
                        return "项目信息";
                    case "TargetInfo":
                        return "指标信息";
                    case "ProjectImage":
                        return "项目图片信息";
                    default:
                        return fieldName;
                }
            }
            // 传入的是关系名
            if (Relations.Contains(fieldName))
            {
                switch (fieldName)
                {
                    case "ProjectInfo_TargetInfo":
                        return "项目信息_指标信息";
                    case "PersonalInfo_DeptInfo":
                        return "个人信息_科室信息";
                    case "PersonalInfo_DiagnosisInfo":
                        return "个人信息_诊断信息";
                    case "DeptInfo_ProjectInfo":
                        return "科室信息_项目信息";
                    case "ProjectInof_ProjectImage":
                        return "项目信息_图片信息";
                    default:
                        return fieldName;
                }
            }

            // 传入的是字段名
            if (Tables[tableName].Columns.Contains(fieldName))
            {
                return Tables[tableName].Columns[fieldName].Caption;
            }

            // 未知
            return fieldName;
        }
    }

}
