﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace KingT.PEIS.Report
{
    public partial class DefaultPacsProjectNew : DevExpress.XtraReports.UI.XtraReport
    {
        public DefaultPacsProjectNew()
        {
            InitializeComponent();
        }

        private void xrPictureBox1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            var pb = sender as XRPictureBox;
            if (pb == null) return;
            pb.Visible = DetailReport1.GetCurrentColumnValue("IMAGE") != null;
        }

    }
}
