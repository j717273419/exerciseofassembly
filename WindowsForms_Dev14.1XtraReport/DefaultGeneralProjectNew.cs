﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace WindowsForms_Dev14._1XtraReport
{
    public partial class DefaultGeneralProjectNew : DevExpress.XtraReports.UI.XtraReport
    {
        private TopMarginBand topMarginBand1;
        private DetailBand detailBand1;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell5;
        private BottomMarginBand bottomMarginBand1;
        private DetailReportBand DetailReport;
        private DetailBand Detail;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private PersonalReportDataSet personalReportDataSet1;
        private TopMarginBand topMarginBand2;
        private DetailBand detailBand2;
        private BottomMarginBand bottomMarginBand2;
        private XRTable xrTable3;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell8;
        private DetailReportBand DetailReport1;
        private DetailBand Detail1;
        private DetailReportBand DetailReport2;
        private DetailBand Detail2;
        private PersonalReportDataSet personalReportDataSet2;
        private ReportHeaderBand ReportHeader;

        public DefaultGeneralProjectNew()
        {
            InitializeComponent();
        }

        private void Detail1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // FillDataSource();
        }

        private void Detail2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //FillDataSource();
        }

        private void InitializeComponent()
        {
            this.topMarginBand2 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.detailBand2 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.bottomMarginBand2 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.DetailReport2 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.personalReportDataSet2 = new WindowsForms_Dev14._1XtraReport.PersonalReportDataSet();
            this.dataSet11 = new WindowsForms_Dev14._1XtraReport.DataSet1();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personalReportDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // topMarginBand2
            // 
            this.topMarginBand2.HeightF = 100F;
            this.topMarginBand2.Name = "topMarginBand2";
            // 
            // detailBand2
            // 
            this.detailBand2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.detailBand2.HeightF = 100F;
            this.detailBand2.Name = "detailBand2";
            // 
            // xrTable3
            // 
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(30.83333F, 30.83333F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(300F, 25F);
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell8});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.Age")});
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Weight = 1D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.UserName")});
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Weight = 1D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.Remark")});
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Weight = 1D;
            // 
            // bottomMarginBand2
            // 
            this.bottomMarginBand2.HeightF = 100F;
            this.bottomMarginBand2.Name = "bottomMarginBand2";
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.DetailReport2});
            this.DetailReport1.DataSource = this.dataSet11;
            this.DetailReport1.Expanded = false;
            this.DetailReport1.Level = 0;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail1
            // 
            this.Detail1.Expanded = false;
            this.Detail1.HeightF = 100F;
            this.Detail1.Name = "Detail1";
            // 
            // DetailReport2
            // 
            this.DetailReport2.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2});
            this.DetailReport2.DataSource = this.dataSet11;
            this.DetailReport2.Expanded = false;
            this.DetailReport2.Level = 0;
            this.DetailReport2.Name = "DetailReport2";
            // 
            // Detail2
            // 
            this.Detail2.HeightF = 100F;
            this.Detail2.Name = "Detail2";
            // 
            // personalReportDataSet2
            // 
            this.personalReportDataSet2.DataSetName = "PersonalReportData";
            this.personalReportDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet11
            // 
            this.dataSet11.DataSetName = "DataSet1";
            this.dataSet11.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // DefaultGeneralProjectNew
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.topMarginBand2,
            this.detailBand2,
            this.bottomMarginBand2,
            this.DetailReport1});
            this.DataMember = "DataTable1";
            this.DataSource = this.dataSet11;
            this.Version = "14.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personalReportDataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
    }
}
