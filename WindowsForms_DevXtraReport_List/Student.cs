﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsForms_DevXtraReport_List
{
    public class Student
    {
        private string username;
        private int age;

        public string Username
        {
            get
            {
                return username;
            }

            set
            {
                username = value;
            }
        }

        public int Age
        {
            get
            {
                return age;
            }

            set
            {
                age = value;
            }
        }
    }
}
