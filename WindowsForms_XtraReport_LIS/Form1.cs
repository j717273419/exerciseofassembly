﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using System.Windows.Forms;

namespace WindowsForms_XtraReport_LIS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void simpleButton_Print_Click(object sender, EventArgs e)
        {
            XtraReport_Sample report = new XtraReport_Sample(GetDataset());

            report.ShowPreviewDialog();
        }

        public DataSet GetDataset()
        {
            //dataset ds
            DataSet ds = new DataSet();

            //Title
            DataTable TableTitle = new DataTable("Title");
            DataColumn dc1 = new DataColumn("age", typeof(Int32));
            DataColumn dc2 = new DataColumn("username", typeof(string));
            DataColumn dc3 = new DataColumn("sex", typeof(string));
            DataColumn dc4 = new DataColumn("remark", typeof(string));
            DataColumn dc5 = new DataColumn("department", typeof(string));
            DataColumn dc6 = new DataColumn("doctor", typeof(string));
            DataColumn dc7 = new DataColumn("diagnosis", typeof(string));
            DataColumn dc8 = new DataColumn("type", typeof(string));
            DataColumn dc9 = new DataColumn("arrivalDate", typeof(DateTime));
            DataColumn dc10 = new DataColumn("collectionTime", typeof(DateTime));
            TableTitle.Columns.AddRange(new DataColumn[] {
                    dc1,dc2,dc3,dc4,dc5,
                    dc6,dc7,dc8,dc9,dc10
            });

            //result
            DataTable TableResult = new DataTable("Result");
            DataColumn colNO = new DataColumn("no", typeof(Int32));
            DataColumn colItem = new DataColumn("item", typeof(string));
            DataColumn colResult = new DataColumn("result", typeof(string));

            TableResult.Columns.AddRange(new DataColumn[] { colNO, colItem, colResult });

            //Footer
            DataTable TableFooter = new DataTable("Footer");
            DataColumn dcA = new DataColumn("operator", typeof(string));
            DataColumn dcB = new DataColumn("auditor", typeof(string));
            DataColumn dcC = new DataColumn("reportTime", typeof(DateTime));
            TableFooter.Columns.AddRange(new[] { dcA, dcB, dcC });

            ds.Tables.AddRange(new DataTable[] {
                TableTitle,TableResult,TableFooter
            });


            //Fill Data
            ds.Tables["Title"].Rows.Add(
                30, "李小明", "男", "测试样本", "生化检验科",
                "朱医生", "全部阴性", "全血", "2016-11-14 10:20:31", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                );

            for (int i = 0; i < 50; i++)
            {
                ds.Tables["Result"].Rows.Add(
                                i + 1, "检查项目" + (i + 1), "正常"
                                );
            }

            ds.Tables["Footer"].Rows.Add("章子怡", "周润发", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            return ds;
        }
    }
}
