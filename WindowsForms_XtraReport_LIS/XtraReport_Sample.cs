﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace WindowsForms_XtraReport_LIS
{
    public partial class XtraReport_Sample : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport_Sample()
        {
            InitializeComponent();
        }

        public XtraReport_Sample(DataSet ds)
        {
            InitializeComponent();


            this.DataSource = ds;
            this.DataMember = "Result";

            //title
            xrTableCell_UserName.Text = ds.Tables["Title"].Rows[0]["username"].ToString();
            xrTableCell_Sex.Text = ds.Tables["title"].Rows[0]["sex"].ToString();
            xrTableCell_Age.Text = ds.Tables["title"].Rows[0]["age"].ToString();
            xrTableCell_Remark.Text = ds.Tables["title"].Rows[0]["remark"].ToString();
            xrTableCell_Department.Text = ds.Tables["title"].Rows[0]["department"].ToString();
            xrTableCell_Doctor.Text = ds.Tables["title"].Rows[0]["doctor"].ToString();
            xrTableCell_CollectionTime.Text = ds.Tables["title"].Rows[0]["collectiontime"].ToString();
            xrTableCell_Type.Text = ds.Tables["title"].Rows[0]["type"].ToString();
            xrTableCell_Diagnosis.Text = ds.Tables["title"].Rows[0]["diagnosis"].ToString();
            xrTableCell_ArrivalDate.Text = ds.Tables["title"].Rows[0]["arrivaldate"].ToString();

            //result
            xrLabel_No.DataBindings.Add("Text", ds.Tables["Result"], "no");
            xrLabel_Item.DataBindings.Add("Text", ds.Tables["Result"], "item");
            xrLabel_Result.DataBindings.Add("Text", ds.Tables["Result"], "result");
            //footer
            xrTableCell_Operator.Text = ds.Tables["footer"].Rows[0]["operator"].ToString();
            xrTableCell_Auditor.Text = ds.Tables["footer"].Rows[0]["auditor"].ToString();
            xrTableCell_ReportTime.Text = ds.Tables["footer"].Rows[0]["reporttime"].ToString();

        }
    }
}
