﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace wf_repx
{
    public partial class XtraReport1 : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport1()
        {
            InitializeComponent();
        }
        public XtraReport1(DataSet ds)
        {
            InitializeComponent();
            xrLabel_Title.Text = ds.Tables["Title"].Rows[0]["title"].ToString();
            xrLabel1.Text = ds.Tables["Title"].Rows[0]["username"].ToString();
        }
    }
}
