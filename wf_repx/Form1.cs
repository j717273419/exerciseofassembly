﻿using DevExpress.XtraReports.Extensions;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;

namespace wf_repx
{
    public partial class Form1 : Form
    {
        DataSetRepx ds = new DataSetRepx();

        public Form1()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //组织datatable数据
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("title");
            ds.Tables.Add(dt);
            DataColumn dc1 = new DataColumn("title", typeof(string));
            DataColumn dc2 = new DataColumn("username", typeof(string));
            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            dt.Rows.Add("daohe", "119");
            dt.Rows.Add("daohe", "229");
            dt.Rows.Add("daohe", "339");
            XtraReport1 xr = new XtraReport1(ds);
            xr.ShowPreviewDialog();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Form_ReportDesigner reportDesigner = new Form_ReportDesigner();
            reportDesigner.Show();
        }

        //调用 repx报表
        private void simpleButton3_Click(object sender, EventArgs e)
        {
            XtraReport report = CreateReportFromFile("report1.repx");
            report.ShowPreviewDialog();
        }

        // Create a report from a file. 
        private XtraReport CreateReportFromFile(string filePath)
        {
            XtraReport report = XtraReport.FromFile(filePath, true);
            return report;
        }



        //保存repx报表
        private void simpleButton4_Click(object sender, EventArgs e)
        {

        }

        //加载repx到报表设计器
        private void simpleButton5_Click(object sender, EventArgs e)
        {
            //组织datatable数据
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("title");
            ds.Tables.Add(dt);
            DataColumn dc1 = new DataColumn("title", typeof(string));
            DataColumn dc2 = new DataColumn("username", typeof(string));
            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            dt.Rows.Add("daohe", "119");
            dt.Rows.Add("daohe", "229");
            dt.Rows.Add("daohe", "339");
            XtraReport report = CreateReportFromFile("report1.repx");
            report.DataSource = ds;
            report.ShowDesignerDialog();
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            //组织datatable数据
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("title");
            ds.Tables.Add(dt);
            DataColumn dc1 = new DataColumn("title", typeof(string));
            DataColumn dc2 = new DataColumn("username", typeof(string));
            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            dt.Rows.Add("daohe", "119");
            dt.Rows.Add("daohe", "229");
            dt.Rows.Add("daohe", "339");

            XtraReport report = CreateReportFromFile("report2.repx");
            report.DataSource = ds;
            report.ShowPreviewDialog();
        }

        //调用Stream中的repx报表
        private void simpleButton8_Click(object sender, EventArgs e)
        {

            //byte[] ff = Encoding.Default.GetBytes(ds.Tables[0].Rows[0]["ReportStream"].ToString());
            //MemoryStream ms = new MemoryStream(ff);
            //MemoryStream stream = new MemoryStream();
            //IFormatter formatter = new BinaryFormatter();
            //formatter.Serialize(stream, ds.Tables[0].Rows[0]["ReportStream"]);
            //byte[] bytes = stream.GetBuffer();
            //MemoryStream stream2 = new MemoryStream(bytes);

            //1.
            //XtraReport report = CreateReportFromFile("report2.repx");
            //var stream = StoreReportToStream(report);
            //XtraReport report2 = XtraReport.FromStream(stream, true);
            //report2.DataSource = GetDataSet();
            //report2.ShowDesignerDialog();

            //2.
            byte[] bytes = (byte[])ds.Tables[0].Rows[0]["ReportStream"];
            MemoryStream stream = new MemoryStream(bytes);
            XtraReport report2 = XtraReport.FromStream(stream, true);
            report2.DataSource = GetDataSet();
            report2.ShowDesignerDialog();
        }

        //保存repx报表到Stream
        private void simpleButton7_Click(object sender, EventArgs e)
        {

            XtraReport report = CreateReportFromFile("report2.repx");
            byte[] bytes = StoreReportToStream(report).ToArray();
            ds.Tables[0].Rows.Add("11", "report11", bytes);

        }
        // Save a report to a stream.
        private MemoryStream StoreReportToStream(XtraReport report)
        {
            MemoryStream stream = new MemoryStream();
            report.SaveLayout(stream);
            return stream;
        }

        public DataSet GetDataSet()
        {
            //组织datatable数据
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("title");
            ds.Tables.Add(dt);
            DataColumn dc1 = new DataColumn("title", typeof(string));
            DataColumn dc2 = new DataColumn("username", typeof(string));
            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            dt.Rows.Add("daohe", "119");
            dt.Rows.Add("daohe", "229");
            dt.Rows.Add("daohe", "339");

            return ds;
        }
    }
}
