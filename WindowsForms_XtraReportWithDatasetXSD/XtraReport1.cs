﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace WindowsForms_XtraReportWithDatasetXSD
{
    public partial class XtraReport1 : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport1()
        {
            InitializeComponent();

            //dataSet13.Tables[0].Rows.Add("daohe", 123);
            //dataSet13.Tables[0].Rows.Add("daohe", 123);
            //dataSet13.Tables[0].Rows.Add("daohe", 123);
            //dataSet13.Tables[0].Rows.Add("daohe", 123);
            //dataSet13.Tables[0].Rows.Add("daohe", 123);
            //组织datatable数据
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("student");
            ds.Tables.Add(dt);
            DataColumn dc1 = new DataColumn("username", typeof(string));
            DataColumn dc2 = new DataColumn("age", typeof(Int32));
            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            dt.Rows.Add("daohe1", 116);
            dt.Rows.Add("daohe2", 227);
            dt.Rows.Add("daohe3", 338);

            this.DataSource = ds;
            this.DataMember = "student";
            xrLabel_Name.DataBindings.Add("Text", ds, "username");
            xrLabel_Age.DataBindings.Add("Text", ds, "age");



        }

        public XtraReport1(DataSet ds)
        {
            InitializeComponent();

            this.DataSource = ds;
            this.DataMember = "student";
            xrLabel_Name.DataBindings.Add("Text", ds, "username");
            xrLabel_Age.DataBindings.Add("Text", ds, "age");
        }

    }
}
