﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsForms_XtraReportWithDatasetXSD
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            XtraReport1 report = new XtraReport1();
            report.ShowPreviewDialog();
            report.FillDataSource();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            //组织datatable数据
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("student");
            ds.Tables.Add(dt);
            DataColumn dc1 = new DataColumn("username", typeof(string));
            DataColumn dc2 = new DataColumn("age", typeof(Int32));
            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            dt.Rows.Add("daohe1", 1166);
            dt.Rows.Add("daohe2", 2266);
            dt.Rows.Add("daohe3", 3366);
            XtraReport1 report = new XtraReport1(ds);
            report.ShowPreviewDialog();
            report.FillDataSource();
        }
    }
}
