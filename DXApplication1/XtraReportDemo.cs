﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace DXApplication1
{
    public partial class XtraReportDemo : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReportDemo()
        {
            InitializeComponent();

            //组织datatable数据
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("student");
            ds.Tables.Add(dt);
            DataColumn dc1 = new DataColumn("username", typeof(string));
            DataColumn dc2 = new DataColumn("age", typeof(Int32));
            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            dt.Rows.Add("daohe1", 11);
            dt.Rows.Add("daohe2", 22);
            dt.Rows.Add("daohe3", 33);

            this.DataSource = ds;
            this.DataMember = "student";
            xrLabel_Name.DataBindings.Add("Text", ds, "username");
            xrLabel_Age.DataBindings.Add("Text", ds, "age");



        }

        public XtraReportDemo(DataSet ds)
        {
            InitializeComponent();

            this.DataSource = ds;
            this.DataMember = "student";
            xrLabel_Name.DataBindings.Add("Text", ds, "username");
            xrLabel_Age.DataBindings.Add("Text", ds, "age");
        }

    }
}
