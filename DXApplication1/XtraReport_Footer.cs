﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace DXApplication1
{
    public partial class XtraReport_Footer : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport_Footer()
        {
            InitializeComponent();
        }
        public XtraReport_Footer(DataSet ds)
        {
            InitializeComponent();

            //footer
            xrTableCell_Operator.Text = ds.Tables["footer"].Rows[0]["operator"].ToString();
            xrTableCell_Auditor.Text = ds.Tables["footer"].Rows[0]["auditor"].ToString();
            xrTableCell_ReportTime.Text = ds.Tables["footer"].Rows[0]["reporttime"].ToString();
        }

    }
}
