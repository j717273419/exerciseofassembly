﻿namespace DXApplication1
{
    partial class XtraReport_1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel_Result = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_No = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_Item = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrSubreport_BasicInfo = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport_Title = new DevExpress.XtraReports.UI.XRSubreport();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport_Footer = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrTable_Name = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_Name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_Result,
            this.xrLabel_No,
            this.xrLabel_Item});
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 27.5F;
            this.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnCount;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel_Result
            // 
            this.xrLabel_Result.BorderColor = System.Drawing.Color.White;
            this.xrLabel_Result.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_Result.Dpi = 100F;
            this.xrLabel_Result.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_Result.LocationFloat = new DevExpress.Utils.PointFloat(376.6666F, 0F);
            this.xrLabel_Result.Name = "xrLabel_Result";
            this.xrLabel_Result.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_Result.SizeF = new System.Drawing.SizeF(273.3334F, 25F);
            this.xrLabel_Result.StylePriority.UseBorderColor = false;
            this.xrLabel_Result.StylePriority.UseBorders = false;
            this.xrLabel_Result.StylePriority.UseFont = false;
            this.xrLabel_Result.StylePriority.UseTextAlignment = false;
            this.xrLabel_Result.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel_No
            // 
            this.xrLabel_No.BorderColor = System.Drawing.Color.White;
            this.xrLabel_No.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_No.Dpi = 100F;
            this.xrLabel_No.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_No.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_No.Name = "xrLabel_No";
            this.xrLabel_No.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_No.SizeF = new System.Drawing.SizeF(100F, 25F);
            this.xrLabel_No.StylePriority.UseBorderColor = false;
            this.xrLabel_No.StylePriority.UseBorders = false;
            this.xrLabel_No.StylePriority.UseFont = false;
            this.xrLabel_No.StylePriority.UseTextAlignment = false;
            this.xrLabel_No.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel_Item
            // 
            this.xrLabel_Item.BorderColor = System.Drawing.Color.White;
            this.xrLabel_Item.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_Item.Dpi = 100F;
            this.xrLabel_Item.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_Item.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrLabel_Item.Name = "xrLabel_Item";
            this.xrLabel_Item.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_Item.SizeF = new System.Drawing.SizeF(276.6666F, 25F);
            this.xrLabel_Item.StylePriority.UseBorderColor = false;
            this.xrLabel_Item.StylePriority.UseBorders = false;
            this.xrLabel_Item.StylePriority.UseFont = false;
            this.xrLabel_Item.StylePriority.UseTextAlignment = false;
            this.xrLabel_Item.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 100F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 100F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_Name,
            this.xrSubreport_BasicInfo,
            this.xrSubreport_Title,
            this.xrSubreport1});
            this.PageHeader.Dpi = 100F;
            this.PageHeader.HeightF = 96.66666F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrSubreport_BasicInfo
            // 
            this.xrSubreport_BasicInfo.Dpi = 100F;
            this.xrSubreport_BasicInfo.LocationFloat = new DevExpress.Utils.PointFloat(0F, 46.00001F);
            this.xrSubreport_BasicInfo.Name = "xrSubreport_BasicInfo";
            this.xrSubreport_BasicInfo.SizeF = new System.Drawing.SizeF(100F, 23F);
            // 
            // xrSubreport_Title
            // 
            this.xrSubreport_Title.Dpi = 100F;
            this.xrSubreport_Title.LocationFloat = new DevExpress.Utils.PointFloat(0F, 23.00001F);
            this.xrSubreport_Title.Name = "xrSubreport_Title";
            this.xrSubreport_Title.SizeF = new System.Drawing.SizeF(100F, 23F);
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport_Footer});
            this.PageFooter.Dpi = 100F;
            this.PageFooter.HeightF = 25F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrSubreport_Footer
            // 
            this.xrSubreport_Footer.Dpi = 100F;
            this.xrSubreport_Footer.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrSubreport_Footer.Name = "xrSubreport_Footer";
            this.xrSubreport_Footer.SizeF = new System.Drawing.SizeF(100F, 23F);
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 100F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = new DXApplication1.XtraReport_Logo();
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(100F, 23F);
            // 
            // xrTable_Name
            // 
            this.xrTable_Name.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_Name.BorderWidth = 2F;
            this.xrTable_Name.Dpi = 100F;
            this.xrTable_Name.Font = new System.Drawing.Font("宋体", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable_Name.LocationFloat = new DevExpress.Utils.PointFloat(0F, 69.00002F);
            this.xrTable_Name.Name = "xrTable_Name";
            this.xrTable_Name.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable_Name.SizeF = new System.Drawing.SizeF(650F, 25F);
            this.xrTable_Name.StylePriority.UseBorders = false;
            this.xrTable_Name.StylePriority.UseBorderWidth = false;
            this.xrTable_Name.StylePriority.UseFont = false;
            this.xrTable_Name.StylePriority.UseTextAlignment = false;
            this.xrTable_Name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell40,
            this.xrTableCell41,
            this.xrTableCell44});
            this.xrTableRow7.Dpi = 100F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Dpi = 100F;
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Text = "序号";
            this.xrTableCell40.Weight = 0.50000004811188958D;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Dpi = 100F;
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Text = "项目";
            this.xrTableCell41.Weight = 1.38333309286946D;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Dpi = 100F;
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Text = "结果";
            this.xrTableCell44.Weight = 1.3666671067937291D;
            // 
            // XtraReport_1
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.PageFooter});
            this.Version = "16.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_Name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRSubreport xrSubreport_Title;
        private DevExpress.XtraReports.UI.XRSubreport xrSubreport1;
        private DevExpress.XtraReports.UI.XRSubreport xrSubreport_BasicInfo;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRSubreport xrSubreport_Footer;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_Result;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_No;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_Item;
        private DevExpress.XtraReports.UI.XRTable xrTable_Name;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
    }
}
