﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace DXApplication1
{
    public partial class XtraReport_BasicInfo : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport_BasicInfo()
        {
            InitializeComponent();
        }
        public XtraReport_BasicInfo(DataSet ds)
        {
            InitializeComponent();

            //basic info
            xrTableCell_UserName.Text = ds.Tables["Title"].Rows[0]["username"].ToString();
            xrTableCell_Sex.Text = ds.Tables["title"].Rows[0]["sex"].ToString();
            xrTableCell_Age.Text = ds.Tables["title"].Rows[0]["age"].ToString();
            xrTableCell_Remark.Text = ds.Tables["title"].Rows[0]["remark"].ToString();
            xrTableCell_Department.Text = ds.Tables["title"].Rows[0]["department"].ToString();
            xrTableCell_Doctor.Text = ds.Tables["title"].Rows[0]["doctor"].ToString();
            xrTableCell_CollectionTime.Text = ds.Tables["title"].Rows[0]["collectiontime"].ToString();
            xrTableCell_Type.Text = ds.Tables["title"].Rows[0]["type"].ToString();
            xrTableCell_Diagnosis.Text = ds.Tables["title"].Rows[0]["diagnosis"].ToString();
            xrTableCell_ArrivalDate.Text = ds.Tables["title"].Rows[0]["arrivaldate"].ToString();
        }
    }
}
