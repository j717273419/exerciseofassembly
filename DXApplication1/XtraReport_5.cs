﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace DXApplication1
{
    public partial class XtraReport_5 : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport_5()
        {
            InitializeComponent();
        }
        public XtraReport_5(DataSet ds)
        {
            InitializeComponent();
            this.DataSource = ds;
            this.DataMember = "result";

            //title
            xrSubreport_Title.ReportSource = new XtraReport_Title(ds);

            //basic info
            xrSubreport_BasicInfo.ReportSource = new XtraReport_BasicInfo(ds);

            //footer
            xrSubreport_Footer.ReportSource = new XtraReport_Footer(ds);

            //result
            xrLabel_No.DataBindings.Add("Text", ds.Tables["Result"], "no");
            xrLabel_Item.DataBindings.Add("Text", ds.Tables["Result"], "item");
            xrLabel_Result.DataBindings.Add("Text", ds.Tables["Result"], "result");
            xrLabel_Reference.DataBindings.Add("Text", ds.Tables["Result"], "reference");
            xrLabel_Code.DataBindings.Add("Text", ds.Tables["Result"], "code");
            xrLabel_Tips.DataBindings.Add("Text", ds.Tables["Result"], "tips");
            xrLabel_Unit.DataBindings.Add("Text", ds.Tables["Result"], "unit");
        }

    }
}
