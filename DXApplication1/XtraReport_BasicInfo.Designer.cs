﻿namespace DXApplication1
{
    partial class XtraReport_BasicInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrTable_Title = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_Sex = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_Age = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_Remark = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_Department = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_Doctor = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_CollectionTime = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_Diagnosis = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_Type = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_ArrivalDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_UserName = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_Title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_Title});
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 75.50001F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 100F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 100F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable_Title
            // 
            this.xrTable_Title.Dpi = 100F;
            this.xrTable_Title.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable_Title.Name = "xrTable_Title";
            this.xrTable_Title.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5,
            this.xrTableRow3,
            this.xrTableRow4});
            this.xrTable_Title.SizeF = new System.Drawing.SizeF(650F, 75F);
            this.xrTable_Title.StylePriority.UseTextAlignment = false;
            this.xrTable_Title.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell_UserName,
            this.xrTableCell27,
            this.xrTableCell_Sex,
            this.xrTableCell29,
            this.xrTableCell_Age,
            this.xrTableCell32,
            this.xrTableCell_Remark});
            this.xrTableRow5.Dpi = 100F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 100F;
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Text = "姓名：";
            this.xrTableCell25.Weight = 0.38095241728283114D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Dpi = 100F;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Text = "性别：";
            this.xrTableCell27.Weight = 0.40873013450985862D;
            // 
            // xrTableCell_Sex
            // 
            this.xrTableCell_Sex.Dpi = 100F;
            this.xrTableCell_Sex.Name = "xrTableCell_Sex";
            this.xrTableCell_Sex.Weight = 0.39682529994419635D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Dpi = 100F;
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Text = "年龄：";
            this.xrTableCell29.Weight = 0.230158923921131D;
            // 
            // xrTableCell_Age
            // 
            this.xrTableCell_Age.Dpi = 100F;
            this.xrTableCell_Age.Name = "xrTableCell_Age";
            this.xrTableCell_Age.Weight = 0.44841264997209818D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Dpi = 100F;
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Text = "备注：";
            this.xrTableCell32.Weight = 0.26587291899181553D;
            // 
            // xrTableCell_Remark
            // 
            this.xrTableCell_Remark.Dpi = 100F;
            this.xrTableCell_Remark.Name = "xrTableCell_Remark";
            this.xrTableCell_Remark.Weight = 0.49603184291294644D;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell_Department,
            this.xrTableCell15,
            this.xrTableCell_Doctor,
            this.xrTableCell19,
            this.xrTableCell_CollectionTime,
            this.xrTableCell21});
            this.xrTableRow3.Dpi = 100F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Dpi = 100F;
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Text = "科别：";
            this.xrTableCell13.Weight = 0.38095241728283114D;
            // 
            // xrTableCell_Department
            // 
            this.xrTableCell_Department.Dpi = 100F;
            this.xrTableCell_Department.Name = "xrTableCell_Department";
            this.xrTableCell_Department.Weight = 0.46825390770321795D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 100F;
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Text = "送检医生：";
            this.xrTableCell15.Weight = 0.40873013450985862D;
            // 
            // xrTableCell_Doctor
            // 
            this.xrTableCell_Doctor.Dpi = 100F;
            this.xrTableCell_Doctor.Name = "xrTableCell_Doctor";
            this.xrTableCell_Doctor.Weight = 0.62698422386532726D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Dpi = 100F;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Text = "采集时间：";
            this.xrTableCell19.Weight = 0.44841264997209829D;
            // 
            // xrTableCell_CollectionTime
            // 
            this.xrTableCell_CollectionTime.Dpi = 100F;
            this.xrTableCell_CollectionTime.Name = "xrTableCell_CollectionTime";
            this.xrTableCell_CollectionTime.Weight = 0.75238095238095226D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Dpi = 100F;
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Weight = 0.00952380952380958D;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22,
            this.xrTableCell_Diagnosis,
            this.xrTableCell24,
            this.xrTableCell_Type,
            this.xrTableCell37,
            this.xrTableCell_ArrivalDate,
            this.xrTableCell39});
            this.xrTableRow4.Dpi = 100F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 100F;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Text = "临床诊断：";
            this.xrTableCell22.Weight = 0.380952380952381D;
            // 
            // xrTableCell_Diagnosis
            // 
            this.xrTableCell_Diagnosis.Dpi = 100F;
            this.xrTableCell_Diagnosis.Name = "xrTableCell_Diagnosis";
            this.xrTableCell_Diagnosis.Weight = 0.46825394403366805D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Dpi = 100F;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Text = "标本类型：";
            this.xrTableCell24.Weight = 0.40873013450985851D;
            // 
            // xrTableCell_Type
            // 
            this.xrTableCell_Type.Dpi = 100F;
            this.xrTableCell_Type.Name = "xrTableCell_Type";
            this.xrTableCell_Type.Weight = 0.62698422386532726D;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Dpi = 100F;
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Text = "送检日期：";
            this.xrTableCell37.Weight = 0.44841264997209829D;
            // 
            // xrTableCell_ArrivalDate
            // 
            this.xrTableCell_ArrivalDate.Dpi = 100F;
            this.xrTableCell_ArrivalDate.Name = "xrTableCell_ArrivalDate";
            this.xrTableCell_ArrivalDate.Weight = 0.75238095238095226D;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Dpi = 100F;
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Weight = 0.00952380952380958D;
            // 
            // xrTableCell_UserName
            // 
            this.xrTableCell_UserName.Dpi = 100F;
            this.xrTableCell_UserName.Name = "xrTableCell_UserName";
            this.xrTableCell_UserName.Weight = 0.46825390770321795D;
            // 
            // XtraReport_BasicInfo
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Version = "16.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_Title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRTable xrTable_Title;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_Sex;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_Age;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_Remark;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_Department;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_Doctor;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_CollectionTime;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_Diagnosis;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_Type;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_ArrivalDate;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_UserName;
    }
}
