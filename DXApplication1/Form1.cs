﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DXApplication1
{
    public partial class Form1 : DevExpress.XtraEditors.XtraForm
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            XtraReportDemo xr = new XtraReportDemo();
            xr.ShowPreviewDialog();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            //组织datatable数据
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("student");
            ds.Tables.Add(dt);
            DataColumn dc1 = new DataColumn("username", typeof(string));
            DataColumn dc2 = new DataColumn("age", typeof(Int32));
            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            dt.Rows.Add("daohe999", 119);
            dt.Rows.Add("daohe888", 229);
            dt.Rows.Add("daohe777", 339);
            XtraReportDemo xr = new XtraReportDemo(ds);
            xr.ShowPreviewDialog();

        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            XtraReport_Sample report = new XtraReport_Sample(GetDataset());

            report.ShowPreviewDialog();
        }
        public DataSet GetDataset()
        {
            //dataset ds
            DataSet ds = new DataSet();

            //Title
            DataTable TableTitle = new DataTable("Title");
            DataColumn dc1 = new DataColumn("age", typeof(Int32));
            DataColumn dc2 = new DataColumn("username", typeof(string));
            DataColumn dc3 = new DataColumn("sex", typeof(string));
            DataColumn dc4 = new DataColumn("remark", typeof(string));
            DataColumn dc5 = new DataColumn("department", typeof(string));
            DataColumn dc6 = new DataColumn("doctor", typeof(string));
            DataColumn dc7 = new DataColumn("diagnosis", typeof(string));
            DataColumn dc8 = new DataColumn("type", typeof(string));
            DataColumn dc9 = new DataColumn("arrivalDate", typeof(DateTime));
            DataColumn dc10 = new DataColumn("collectionTime", typeof(DateTime));
            DataColumn dc11 = new DataColumn("title", typeof(string));
            TableTitle.Columns.AddRange(new DataColumn[] {
                    dc1,dc2,dc3,dc4,dc5,
                    dc6,dc7,dc8,dc9,dc10,
                    dc11
            });

            //result
            DataTable TableResult = new DataTable("Result");
            DataColumn colNO = new DataColumn("no", typeof(Int32));
            DataColumn colItem = new DataColumn("item", typeof(string));
            DataColumn colResult = new DataColumn("result", typeof(string));
            DataColumn colReference = new DataColumn("reference", typeof(string));
            DataColumn colUnit = new DataColumn("unit", typeof(string));
            DataColumn colTips = new DataColumn("tips", typeof(string));
            DataColumn colCode = new DataColumn("code", typeof(string));

            TableResult.Columns.AddRange(new DataColumn[] {
                colNO, colItem, colResult,colReference,colUnit,colTips,colCode });

            //Footer
            DataTable TableFooter = new DataTable("Footer");
            DataColumn dcA = new DataColumn("operator", typeof(string));
            DataColumn dcB = new DataColumn("auditor", typeof(string));
            DataColumn dcC = new DataColumn("reportTime", typeof(DateTime));
            TableFooter.Columns.AddRange(new[] { dcA, dcB, dcC });

            ds.Tables.AddRange(new DataTable[] {
                TableTitle,TableResult,TableFooter
            });


            //Fill Data
            ds.Tables["Title"].Rows.Add(
                30, "李小明", "男", "测试样本", "生化检验科",
                "朱医生", "全部阴性", "全血", "2016-11-14 10:20:31", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                "血型检验报告单"
                );

            for (int i = 0; i < 60; i++)
            {
                ds.Tables["Result"].Rows.Add(
                                i + 1, "检查项目" + (i + 1), "正常", "参考值", "单位", "提示", "ABCD"
                                );
            }

            ds.Tables["Footer"].Rows.Add("章子怡", "周润发", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            return ds;
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            XtraReport_Title report = new XtraReport_Title(GetDataset());
            report.ShowPreview();
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            XtraReport_1 report = new XtraReport_1(GetDataset());
            report.ShowPreview();
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            XtraReport_2 report = new XtraReport_2(GetDataset());
            report.ShowPreview();
        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            XtraReport_5 report = new XtraReport_5(GetDataset());
            report.ShowPreview();
        }
    }
}
