﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace DXApplication1
{
    public partial class XtraReport_Title : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport_Title()
        {
            InitializeComponent();
        }

        public XtraReport_Title(DataSet ds)
        {
            InitializeComponent();
            xrLabel_Title.Text = ds.Tables["Title"].Rows[0]["title"].ToString();
        }
    }
}
