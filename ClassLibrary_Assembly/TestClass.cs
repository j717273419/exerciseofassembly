﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary_Assembly
{
    public class TestClass
    {
        public string UserName = "wang dongjie";
        public int Age = 18;
        public string GetTime()
        {
            return DateTime.Now.ToString();
        }
        public void Show_UserName(string str)
        {
            Console.WriteLine("--------------Class Library Output-----------------");
            Console.WriteLine(str);
        }
    }
}
